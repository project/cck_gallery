<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>

  <div class="content clear-block">
    <?php print $content ?>
  </div>

  <div class="clear-block">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>

<?php if (module_exists('cck_gallery')): ?>
<div id="gallery-<?php print $node->nid; ?>" class="cck-gallery">
<?php
  $view = variable_get('cck_gallery_contents_view', 'gallery_contents');
  $display = variable_get('cck_gallery_contents_view_display', 'default');
  print views_embed_view($view, $display, $node->nid);
?>
</div>
<?php endif; ?>
<?php
/**
 * Build the admin settings form.
 */
include_once(drupal_get_path('module', 'views').'/includes/admin.inc');

function cck_gallery_admin_form($form_state) {
  
  //load list of views in to array for select lists
  $views = views_get_all_views();
  foreach ($views as $view) {
    $viewnames[$view->name] = $view->name;
  }
  
  //load available view display types in to array for select lists
  $default_display = array('default' => 'Default');
  $other_displays = views_fetch_plugin_names('display');
  $displays = array_merge($default_display, $other_displays);
  
  //load available content types in to array for select list
  $content_types = array_keys(node_get_types());
  
  //content type selectors
  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type selections'),
    '#description' => t('The module installs content types for you to use, but this interface allows you to select custom/different content types should you so wish.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['content_types']['gallery_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the content type to use for galleries'),
    '#options' => $content_types,
    '#default_value' => array_search(variable_get('cck_gallery_gallery_type', 'cck_gallery'), $content_types),
  );
  $form['content_types']['image_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Select the content type to use for images'),
    '#description' => t('Be aware this content type must have two CCK fields - field_image and field_gallery.'),
    '#options' => $content_types,
    '#default_value' => array_search(variable_get('cck_gallery_image_type', 'cck_image'), $content_types),
  );
  
  //gallery view options
  $form['gallery_view'] = array(
    '#type' => 'fieldset',
    '#title' => t('Gallery view selection'),
    '#description' => t('The module provides a default view for galleries, which you may override in the database. You may also prefer to select a different view, using these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['gallery_view']['gallery_default_view'] = array(
    '#type' => 'select',
    '#title' => t('Select the view to be used for displaying thumbnails in a gallery'),
    '#options' => $viewnames,
    '#default_value' => variable_get('cck_gallery_contents_view', 'gallery_contents'),
  );
  $form['gallery_view']['gallery_default_view_display'] = array(
    '#type' => 'select',
    '#title' => t('Select the display of the gallery view to be used'),
    '#options' => $displays,
    '#default_value' => variable_get('cck_gallery_contents_view_display', 'default'),
  );
  
  //user gallery list options
  $form['list_view'] = array(
    '#type' => 'fieldset',
    '#title' => t('Gallery list view selection'),
    '#description' => t('The module provides a default view for lists of galleries belonging to a user, which you may override in the database. You may also prefer to select a different view, using these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['list_view']['gallery_default_list_view'] = array(
    '#type' => 'select',
    '#title' => t('Select the view to be used for displaying the list of user galleries'),
    '#options' => $viewnames,
    '#default_value' => variable_get('cck_gallery_list_view', 'gallery_list_page'),
  );
  $form['list_view']['gallery_default_list_view_display'] = array(
    '#type' => 'select',
    '#title' => t('Select the display of the gallery list to be used'),
    '#options' => $displays,
    '#default_value' => variable_get('cck_gallery_list_view_display', 'default'),
  );
  
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save configuration',
  );
  return $form;
}

function cck_gallery_admin_form_submit($form, &$form_state) {
  //set content types
  variable_set('cck_gallery_gallery_type', $form['content_types']['gallery_content_type']['#options'][$form['content_types']['gallery_content_type']['#value']]);
  variable_set('cck_gallery_image_type', $form['content_types']['image_content_type']['#options'][$form['content_types']['image_content_type']['#value']]);
  
  variable_set('cck_gallery_contents_view', $form['gallery_view']['gallery_default_view']['#value']);
  //TODO: we need to make sure the selected display exists in a validate funcion
  variable_set('cck_gallery_contents_view_display', $form['gallery_view']['gallery_default_view_display']['#value']);
  
  variable_set('cck_gallery_list_view', $form['list_view']['gallery_default_list_view']['#value']);
  //TODO: we need to make sure the selected display exists in a validate funcion
  variable_set('cck_gallery_list_view_display', $form['list_view']['gallery_default_list_view_display']['#value']);
  
  drupal_set_message(t('Settings saved.'));
}
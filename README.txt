Install CCK Gallery in the usual way. Dependencies are:

CCK
Nodereference
Imagefield
Filefield
Content Copy
Views
ImageCache
Image API

Suggested additional modules for stronger permissions management are:

Content Access
ACL

NOTE there is no integration for the above just yet. TODO!

Copy the node template in the theme directory to your theme. You need
to do this so the galleries render properly (there is views_embed_view
code in there). If you choose a custom content type in admin, you will
need to apply the same code in a tpl for that content type.

Settings are at admin/content/cck-gallery

Permissions are 'administer cck galleries' (for admin access) and 'view
cck galleries' (for access to cck-galleries/%user gallery lists).

Also plan to do some integration with the User Relationships module for
allowing access by friend list.